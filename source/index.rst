Welcome to Noctis & Aurel documentation!
=======================

**Noctis & Aurel** is a robotics platform library for Makers in the making.
Noctis & Aurel allow you to learn the basics of robotics in general, covering big 
knowledge areas like 3D printing, electronics and programming.

.. image:: /_static/images/noc_aur.png

Check out the :ref:`LoCoVerse <LoCoVerse>` section for a gentle introduction into the LoCo CORP universe, and also :ref:`Noctis & Aurel <Noctis & Aurel>`
for a deeper explanation of what a Noctis & Aurel kit includes.

Noctis & Aurel has a native :ref:`Mobile App <Mobile App>` that allows remote control through bluetooth connection and mounts a :ref:`microbit <Microbit>`
board to ease the programming learning. In the :ref:`MakeCode Tutorials <Use a bluetooth connection>` section, you can see a set of different of MakeCode programming examples and in :ref:`Move your robot <Move your robot>`, you will find how to program movements and dances.

.. note::

   This project is under active development.

Quick Start
-----------

For a quick start, access to the micro:bit board by detaching the robot hood. It is magnetically linked, so just pull it off with a bit of strength. If you
need help for assembling, you can check out the guide in :ref:`Assembly <Assembly tutorial>`.

.. image:: /_static/images/access_microbit.png

As a first example, we will load a ready-to-use code into our robot. In this case, the code will program a simple routine of forward movement, controlling the servos by 
means of the micro:bit code. First, we will need to access the code, available `here <https://makecode.microbit.org/81542-64198-87452-10879>`__. Once you enter the link, 
press “Edit Code” in the right top corner. This will take you to the web editor, where you should see something like this:

.. image:: /_static/images/avance_frontal.png

The code is ready to use, we only need to save it in the micro:bit board. We will need to connect the board to our computer to do so. For this, 
you just need to connect the small end of the USB cable to the micro USB port on the micro:bit, and the other end of the cable to a USB port of your computer.

Once connected, we will use the one-click download to save the code directly into the board. Follow the instructions that will appear on your screen:

.. image:: /_static/images/link_microbit.png

.. #. Click the triple dot icon on the Download button and then click Connect device.
.. #. In the connection message window, click Next.
.. #. Another message window will display telling you which device you should pair with. Click Next to go to the device list.
.. #. Select BBC micro:bit CMSIS-DAP or DAPLink CMSIS-DAP from the list and click Connect.
.. #. When your micro:bit is connected, you'll see the Connected to micro:bit message window. Click on Done and you're ready to go!
.. #. Once connected, just click Download and the code will be transferred to the micro:bit board.

When your micro:bit is connected, you'll see the Connected to micro:bit message window. Click on Done and you're ready to go! Once connected, just click 
Download and the code will be transferred to the micro:bit board.

Insert the micro:bit in the LoCoCarrier and attach the hood with the magnets. Your code will run automatically and your robot will start walking! You can visually see
these steps in the next YouTube video.


.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/o5jTzpHpKBg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>
    


.. toctree::
   :hidden:
   :maxdepth: 0

   index


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Introduction

   pages/locoquad.rst
   pages/locoverse.rst
   pages/microbit.rst
   

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Starting with microbit

   pages/setup_microbit.rst
   pages/lights.rst
   pages/use_buttons.rst
   pages/sensing.rst

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Controlling your robot

   pages/move_robot.rst
   pages/platforms.rst
   pages/peripherals.rst

