if (!String.prototype.includes) {
  String.prototype.includes = function(search, start) {
    'use strict';
    if (typeof start !== 'number') {
      start = 0;
    }

    if (start + search.length > this.length) {
      return false;
    } else {
      return this.indexOf(search, start) !== -1;
    }
  };
}

function addNavbar() {
  var navbarHtml = "" +
    "  <div id=\"lux-global-navbar\" class=\"lux-navbar-container\">\n" +
    "    <div class=\"lux-navbar\">\n" +
    "      <ul>\n" +
    "        <li><a href=\"https://www.lococorp.org/\">\n" +
    "          <img src=\"https://locoquad-documentation.readthedocs.io/en/latest/_static/images/loco_circle.png\" alt=\"LoCo CORP\">\n" +
    "        </a></li>\n" +
    "        <li><a href=\"https://locoquad-documentation.readthedocs.io/en/latest/\" class=\"lux-navbar-active\">Documentation</a></li>\n" +
    "        <li><a href=\"https://www.lococorp.org/pages/locoteam\">Team</a></li>\n" +
    "        <li><a href=\"https://www.lococorp.org/pages/contact\">Contact</a></li>\n" +
    "        <li><a href=\"https://www.lococorp.org/collections/noctis-aurel\">Shop</a></li>\n" +
    "      </ul>\n" +
    "    </div>\n" +
    "  </div>\n"  +
    "  <div id=\"lux-doc-navbar\" class=\"lux-navbar-container\">\n" +
    "    <div class=\"lux-navbar\">\n"  +
    "      <ul>\n" +
    "        <li><a href=\"/en/latest/\">Main</a></li>\n" +
    "      </ul>\n" + 
    "    </div>\n" +
    "  </div>" 
  document.body.insertAdjacentHTML( 'afterbegin', navbarHtml );
}

function adjustNavbarPosition() {
  var navbar = document.getElementsByClassName("wy-nav-side")[0];
  var offset = 146 - window.pageYOffset;
  if (offset >= 0) {
    navbar.style.top = offset + "px";
  } else {
    navbar.style.top = 0 + "px";
  }
}

window.onscroll = adjustNavbarPosition

document.onreadystatechange = function(e) {
  if (document.readyState === 'interactive') {
    addNavbar()
    adjustNavbarPosition()
    document.getElementsByClassName("wy-side-scroll")[0].scrollTop = 0;
    if (location.pathname.startsWith("/projects")) {
      var links = document.querySelectorAll("#lux-doc-navbar a[href^='/projects']");
      for (var i = 0; i < links.length; i++) {
        if (location.pathname.includes(links[i].pathname)) {
          links[i].classList.add('lux-navbar-active')
        }
      }
    } else {
      var main = document.querySelector("#lux-doc-navbar a:not([href^='/projects'])")
      main.classList.add('lux-navbar-active')
    }
  }
}
