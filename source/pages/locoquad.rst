Noctis & Aurel
===================================

Noctis & Aurel robotic replicas are programmable robots based on BBC micro:bit. By creating block programs in microbit, you can make 
your robot acquire a wide range of abilities, such as displaying lights, reacting to sounds or dodging obstacles. At the same
time, it has a PC application based on Unity from where you will be able to see a simulation of the robot and remotely 
control its movements.

Their body is composed of 3D printed parts. These parts are easily designed and interchangeable, to be faithful to the
biodiversity presented by the Amica Quadrupes. You will be able to customize your robot as you wish.

Your robot has 8 servos (2 for each leg) that allow the robot to move in 2 dimensions. The eyes are an ultrasonic 
sensor that allows distance detection. In addition, it has all the sensors incorporated inside the microbit board, which you 
will see in the :ref:`microbit <Microbit>` section. The battery module gives it an autonomy of about 2 hours of intense use.

LoCoQUAD will help you to start your Maker path, assimilating the basics of robotics, electronics and programming.

Components
------------

What is included with a LoCoQuad

.. image:: /_static/images/parts.png

Assembly 
----------------

Avengers!

