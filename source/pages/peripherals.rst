Connect more devices: servos, cameras...
===================================

Using servo connectors
------------

Although servo connectors are labelled like that, they are regular exposed pins,
mapped to source voltage of 5V, ground and the PWM output of a PCA9685, which is an
I2C controlled 16-channel LED controller with 12 bits of resolution. The output can be
configured to be a PWM operating at a frequency from 24Hz to 1526Hz. This means we can
not only connect up to 12 servos, but also connect whatever device working at 5V and
controlled by PWM. Note that being an I2C-bus controlled device, PCA9685 is allocated
on a fixed address, being 0x40 in this case. Most languages have accessible, easy to
use libraries to operate with the PCA9685 module.

Connect your own camera module
------------

We are working on this. If you are eager and want to try by yourself, let us know the results!
