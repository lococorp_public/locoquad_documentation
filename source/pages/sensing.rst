Sense the environment
===================================

In this section we will learn how to use the different sensors to create reactive programs.

Let's check what we can do!.

Temperature
------------

We will start with the temperature measurement. Microbit measures the temperature with its microcontroller's internal sensor. First, we are going to display the temperature measured by the sensor continuously over the LED array. To do this, knowing that the temperature is a number measured in degrees Celsius (ºC), we drag the "show number" block of the "Basic" section in the "forever" block. The number we want to display is the temperature, measured as an input quantity. Therefore, in the "Input" section we can find the field "temperature (ºC)" to enter it in the "show number" block.

.. image:: /_static/images/show_temperature.png

Now that you can see the current temperature, we are also going to implement an alert based on a limit temperature. Firstly, we will emit a beep if the current temperature is greater than 25ºC. To do this, we continuously check if the temperature is above 25 with an "if [condition] then" block that we will find in the "Logic" section. Below this same block we have the comparative conditions, of which we are interested in one of those that compares two numbers using a mathematical sign and in which we will change this sign to a ">". We copy the temperature input on the left side of the comparison and we write 25ºC on the the right. 

.. image:: /_static/images/temp_comparison.png

In the "Music" section we can find different blocks to play sounds, we are going to use "play tone [Tone] for [time]". We choose the highest pitch of the keyboard (high B) and play it for half a beat. We can also set a minimum limit with a different sound, for example, play low C when temperature is below 20ºC.

.. image:: /_static/images/temperature_sound.png

In the previous cases, the temperature was always displayed. Now we will only display it if it lies in the safe range (20-25). Otherwise, we will play a sound and display "hot" or "cold". We will sequentially check if the temperature fulfills the condition, reproducing the code inside if the condition is true or going to the next condition if the previous is false. We do this by extending our "if [condition] then" block by clicking the "+" symbol twice. In the end, your code should look like the one below.

.. image:: /_static/images/temp_if_else.png

It is possible that the temperature measured by your micro:bit does not match the ambient temperature of the room you are in, you can check this with a wall thermometer. This is not because the board is broken or your code is poorly done, but because, as we said, the micro:bit board uses the microcontroller's internal sensor, so the temperature measurement is susceptible to variations in the temperature of the components surrounding the microcontroller, in addition to the microcontroller's own temperature. If it gets too hot, you could observe a measured temperature higher than the ambient temperature. To check that the limits we have chosen are working properly, we can warm up the microcontroller by covering it with a finger (its temperature will rise to match our body temperature which is around 36ºC). If we want to lower its temperature we can put it in the refrigerator for a bit. When its temperature is within the acceptable range we will see the temperature in the LED matrix and when it goes out of the chosen margins we will hear the alert signals. 

Compass
------------

We are going to use the magnetometer built into our micro:bit board to create a compass capable of finding North. It will indicate which direction we are facing and which way North is.

First, weneed to calibrate the compass.The "calibrate compass" block can be found in the "more" section of the "Input" tab. It must be placed inside the "on start" block. As soon as the compass is calibrated, it will already start taking measurements of the orientation with respect to North, so it is as simple as displaying the number measured in sexagesimal degrees on the LED matrix as we have done other times, with the block "show number" and filling the number field with "compass heading (º)" that we find in the "Input" section.

.. image:: /_static/images/compass.png

With this we already have a compass, but to make it more intuitive, we will display the direction we are heading as the figure below. We also add a sound alert for north direction. 

.. image:: /_static/images/directions.png

We divide the 360º into 45º fields, ranging from 22.5*n degrees to 22.5*(n+2) degrees. Thus, the first field (the orange field covering the north-east orientation), goes from 22.5*1 degrees to 22.5*3 degrees (22.5º-67.5º). We are going to make the micro:bit board display the direction in which we are facing (N, E, S, W, NE, SE, SW or NW). For this we will use "if-then" checks and the "Show string" blocks. You will need the boolean blocks to compare two conditions at the same time (greater then 22.5ºC and smaller than 65ºC). They can be found in the "logic" section.

.. image:: /_static/images/full_compass.png

In total we will need 8 conditions: One "if-then", six "else if-then" and one "else". The first condition checks the first possibility, the next six check new possibilities in case the previous ones have not been fulfilled, and finally the "if not" condition is executed without checking a new condition, in case none of the previously checked ones have been verified. This last condition is very useful (and even necessary in some cases), since although it is not performing a specific check, it gives us information about all the checks that have not been verified. As we did before, you can add new conditions to an ifs chain by just clicking on the "+" at the bottom of the block (note that the last condition is always an "else").

The compass is ready, now we can't get lost!

Tilt and acceleration
------------

The accelerometer built into the micro:bit board measures the accelerations to which the board is subjected. This means that we can detect the moments when we rapidly modify its velocity, such as by shaking the board, and we can measure the variations in gravity (which is an acceleration) to know the tilt of the board. 

We are going to take the "on [shake]" block from the "Input" section and we are going to use some of its variants and we are going to give expressions to the micro:bit board to check the detection of those situations. We will also add an alarm sound if the board is falling.

.. image:: /_static/images/tilt.png

To understand how it is possible for the board to detect these situations with the accelerometer alone, we have to think that gravity (isolated from other accelerations) is an acceleration by itself. Therefore, a change in the direction or sense of gravity is also measurable by an acceleration sensor. Can you think of what this might be used for? For a robot balance sensor, perhaps? I'm sure you can think of a few other examples.

Light sensor
------------

We are going to use the LED array for a new function: measuring the intensity of light received. Beaming light onto the LED array causes a voltage drop that we can measure to get information about the intensity of the beamed light. Depending on the light received, we will emit a musical tone. This allows us to check the result without seeing the LED array and therefore we can cover it.

The level of light received is encoded in the micro:bit as a number from 0 (when it receives no light at all) to 255 (when it receives the maximum light it is capable of measuring). This number can be used directly to reproduce a tone of the same Hertz value.

.. image:: /_static/images/light_basic.png

You will notice that the tones from 0 to 255Hz are very low, we can change this with a change of scale. We will use the map function which can be found in "Math". Initially, our variable lie in the 0-255 range. As we see, this is too low, so we would like a new range with higher values. We choose 200-2000, for example. The map function transforms the minimum value (0) in the old range to the minimum value in the new range (200). Similarly, 255 will be mapped to 2000 in the new range. For the numbers in between, they will also be proportionally scaled. 

.. image:: /_static/images/light_tone.png

Now we can turn on the board and listen to the tones. The tones are of higher frequency when the light intensity increases. You can also implement an inverse strategy, with the board emitting lower tones when the light intensity increases.

Noise traffic light
------------

The micro:bit board also has a microphone. We are going to create a noise traffic light, which will create a light signal and emit a buzzing alert when the sound in the room exceeds a set level, while only giving a slight warning if there is some ambient noise and remaining silent if it detects no noise. 

First we need to know when the ambient noise is higher than a chosen level. To do this we are going to create a loop with the "while [condition]" block found in the "Loops" section that checks the audio and compares it to a coded level between 0 and 255. The microphone reading block can be found in the "Input" section.

In this way, whenever the detected sound level is above 100, the chosen action will be executed. In this case, as long as the sound is high, we want to emit an annoying buzzing sound and a red circle, as if it were a stop signal.

.. image:: /_static/images/stop.png

We must remember to turn off the display and the audible alert at times when the loop condition is not met. Otherwise, the alert will stay on, even at times when there is no noise. 

We can also include a second loop as a first warning, indicating that we are approaching the maximum allowable noise level with a less audible hum using the "set volume [audio level]" block. If we do this we must remember to use the same block in the previous loop to change the audio level. The second loop will execute while the sound level is between 50 and 100. We will use booleans and conditionals as we did before.

.. image:: /_static/images/noise_traffic.png

Try clapping in front of the micro:bit to exceed the chosen maximum audio level and see (and hear) the warnings. If you see that after muting the warnings continue, you may have set the level too low or the tone emitted by the board is too loud, so that the board convinces itself that there is noise, even though the noise is being caused by it. Try raising the limit or lowering the volume level.