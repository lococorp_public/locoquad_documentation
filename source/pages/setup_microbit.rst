Use a bluetooth connection
===================================

As shown in the :ref:`Quick Start <Quick Start>` section, you can program your micro:bit by USB connection. The other way to use 
it is with a bluetooth connection, which is what we'll learn here. We start by downloading the app, which you can find both 
in the App Store (Apple) and Play Store (Android).

.. image:: /_static/images/microbit_app.jpg

From the app we are going to pair our micro:bit to our remote device. We select the option “Choose micro:bit” from the menu.

.. image:: /_static/images/microbit_home.png

If it is the first time, we won’t find any already paired micro:bit, so we select “Pair a new micro:bit”.

.. image:: /_static/images/microbit_pair.png

We follow the instructions as they appear on the screen and press simultaneously A, B and RESET buttons while the board is powered 
up (either by USB connected to a computer or batteries). When we have the 3 buttons simultaneously pressed, we release just the 
RESET button, holding A and B. The board will enter pairing mode and will show the bluetooth symbol on the LED matrix, just before 
showing a “random” bar graph. That’s the moment when you can also release A and B and select “Next” on your device.

On your device screen you have to set each bar height so they match the pattern on your micro:bit, like some kind of password.

.. image:: /_static/images/microbit_pattern.png

Your board may ask for an additional code, if not just press “Next” and move on.

If everything went as planned, the board will finish pairing and you will see the success screen. Finally press “OK” on the app 
and RESET on the micro:bit board.

.. image:: /_static/images/microbit_success.png

Go back to the main menu and enter “Create Code” this time. 

.. image:: /_static/images/microbit_create.png

This window is your programming environment. To the left you see a simulation of the real board, where you can check your board’s 
behavior before flashing the program. At the center of the screen you will find a column with a variety of menus containing the 
blocks needed to make your programs. Finally, to the right you can see your program, where we will write our algorithms in the following sections.

.. image:: /_static/images/makecode_screen.png

