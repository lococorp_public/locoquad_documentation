Move your robot
===================================

Finally, we will learn how to implement dances with our robot and make it walk! First, we will start with and introduction about servos and their disposal in the robot, and then we will show some examples!


The basics behind our dances
------------

Your little robot is equipped with 8 servos that act as joints and muscles, enabling movement. Let's see how they are arranged. 

The robot has 4 feet, 2 in the front and 2 in the back. Each of them is composed of a shoulder and a leg, as you can see in the next schematic. It represents a front view of the robot.

.. image:: /_static/images/front_view.png

The servo in the shoulder will rotate the whole foot with respect to the main body, while the servo in the leg will rotate the leg piece with respect to the shoulder. Each servo has a number, and that's how you will act on them in MakeCode. The numeration can be seen in the next image.

.. image:: /_static/images/top_view.png

For example, if you want to move the front right foot, you will have to act on the servos 1 and 2, which correspond to the leg and shoulder of the front right foot, respectively.

Now that you know how they are called, it's time to know how they work! As the joints in our body, the servomotors in our robot can rotate in a certain range of angles. In our case, they will be able to move between 0º and 180º degrees, being 90º the calibration position where the robot lies down as in the previous figure.

More specifically, in the next figure you will see again the top view of the robot, but with the angle references for the shoulders.

.. image:: /_static/images/shoulders.png

You have to be careful with the references when you are planning some moves, as they are a bit tricky. For example, in the case you wanted to move both front legs to the forward direction, as if the robot were extending its feet to the front, you will set the servo 11 (Front shoulder Leg) to the value of 60º, and the servo 2 (Front Shoulder Right) to the value of 120º.

Now let's move to the legs. Their references come in pairs, as you can see in the figure. In both cases, 90º corresponds to the leg being parallel to the robot body. But for the left case, servos 12 and 4 (Front Leg Left and Back Leg Right), the leg will point down to the floor if its value is 180º, while for the right case in the figure, servos 1 and 9, the leg will point down to the floor if its value is 0º.

.. image:: /_static/images/legs.png

Though it may sound a bit confusing, you will see it is really easy with the examples.


Example: push ups
------------

Our robot is full of energy and wants to do a little exercise! Follow the `link <https://makecode.microbit.org/74338-22035-09568-70818>`__ to have access to the code.

.. image:: /_static/images/init_func.png

The 'On start' block will show a heart, followed by the 'init' of the servos and the call to the function 'calib'. A function is just another block we created to group a bunch of commands. In this case, calib sets all servos to the value of 90º, what we call the calibration position. Once this is done, the robot will follow the instructions in the 'forever' block, calling the function pushups indefinetely. Inside the function pushups, we differentiate between two main steps:

.. image:: /_static/images/get_ready.png

This step will put the robot in position to start the pushups. The back feet will be extended to the backward direction, while the front feet will be flexed, ready to do some exercise. Let's start!

.. image:: /_static/images/push.png

The second step is a loop that will perform 4 pushups. As you can see, in the loop we modify the values of servos 1 and 12, corresponding to the Front Leg Right and Left, respectively. The result is and up and down movement that evoques that hard working pushups!

This is your first robot dance! For more examples and inspiration, check out `this repository <https://makecode.microbit.org/08014-25435-90176-74045>`__. Show us what you got and create your own movements!

Example: walking forward
------------

We have prepared a locomotion example, which you cand find `here <https://makecode.microbit.org/71206-74890-20144-76109>`__. Similar to the previous example, we set the servos to their initial positions (not calib in this case) in the 'On start' block, while in the 'Forever' block we will call the function 'avance_frontal' (walk forward).  

.. image:: /_static/images/main_walking.png

The main function is composed of several functions called poses. The concatenation of this different poses creates the flow of movements needed for the locomotion.

.. image:: /_static/images/poses.png

Load the code on your robot and watch his first steps! Now it's your time to add new ideas. Can you make it faster? May you add some sensor inputs to interrupt the walking? It's up to you!