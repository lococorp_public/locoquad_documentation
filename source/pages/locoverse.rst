LoCoVerse
===================================

Noctis & Aurel are robotic representations of an ancient species called Amica Quadrupes. These beings lived long
ago in the planet Proxima Centauri b, located approximately at 4 light years from Earth. 

.. image:: /_static/images/noctis_aurel.png

Proxima Centauri b is tidally locked to its star, which means that it will always have one side of the planet
receiving the star light, while the other is always dark. This rare layout creates two planet sides with opposite
life conditions. Life went thorugh in this planet, giving birth to the Amica Quadrupes species.

However, evolution made its job and forked this species into two different races, forced to adapted to the
different conditions that Proxima Centauri provided. The species Amica Quadrupes bifurcated into the varieties 
Aurel (light side) and Noctis (dark side). The Aurel race witnessed a hatching of life without equal, motivated by the energy that, 
continuously, the star would offer them. On the other side, the Noctis variant, always hidden from the light and heat of the star, 
developed a special interest in the search for brightness and warmth.


The Robot Replicas Project
------------

One year ago, the Earth received the impact of a fragment from this planet. For now, we do not know the phenomenon 
that could have caused this fragmentation and transported the small meteorite, with the size of a car, to the surface of the town of Zaragoza.

A scientific base was established around the impact crater in order to unravel the mysteries surrounding this event. 
LoCo Corporation®, a leading robotics company, is in charge of leading this research and shedding light on these questions. 

At the LoCo CORP base, located in Zaragoza around the remains of the impact, the evolution of Aurel and Noctis is studied 
with the use of robotic replicas. What for many may seem a waste of time and resources, for others is really a struggle 
to recover the diversity of Proxima Centauri b and to be able to study and understand in detail the evolution, nature and, 
ultimately, life in the Universe. All of the information we discovered can be watched in the first chapter of the LoCoVerse.

.. raw:: html

    <div style="position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; height: auto;">
        <iframe src="https://www.youtube.com/embed/gARHGLxpAdU" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe>
    </div>

Build the LoCoVerse with us!
------------

Research is advancing by leaps and bounds, yet the tasks keep piling up and more and more 
scientists and engineers are needed. Fortunately, LoCo CORP has prepared a specific training plan to increase its workforce worldwide. 
We need your abilities to help us, but don't worry, we will help you too. We have plenty of resources ready for you, and you can use it
as you prefer. In these docs, you will find more technical details that may not be covered in the user's manual, which adopts a more general approach.
Our goal is to guide you in your path to become a full-fledged member of LoCo CORP, or, as we like to consider ourselves, a true Maker. 
Along these lines you will discover the basic elements of a robot, what they are, how they work and how they relate to each other. You will give 
life to an Aurel or Noctis replica that will accompany you during this journey, in which you will teach it to walk, make sounds or respond to certain stimuli. 
Best of all, you will learn at the same time, and you will discover the basics of programming, a skill that is in great demand nowadays, 
in an intuitive and interactive way. The world needs engineers, physicists, chemists and mathematicians, now more than ever. 

Also, our world need storytellers, writers, artists and designers. Here you will find stories of Noctis, Aurel, and many more robots 
that will join the LoCoVerse as it expands. We have established the first narrative and aesthetic lines, and we will continue building 
the LoCoVerse, but we don't want it to be exclusively our creation.We have designed it so that all members of the robotics community 
can participate, add their stories, present their robots and enjoy the creative process with us. 

.. image:: /_static/images/create_locoverse.png

Are you ready to join our mission, and will you be able to meet the challenges of technological and scientific learning? 
Like Proxima Centauri b, there are two sides to this experience. There will be times when you feel the cold, the 
loneliness and the frustration of the sometimes arduous path of study. However, on the other side, there will always be 
the warm and rewarding feeling that always accompanies learning. And don't worry, if you need anything, we'll be there to help you. 

Welcome to LoCo CORP.
