Turn all the lights on
===================================

In this section we will learn how to use the LED matrix of the micro:bit in different ways.

As you can see, you have two blocks in the program window. 'On start' will execute the instructions 
inside only once, and it will be the first thing that happens once we run the code. 'Forever' will be executed
right after, and it will run the instructions written inside in an endless loop.

Now we know this, let's move to lighting part.

Write messages
------------

We can print text messages on the screen. Click on the 'Basic menu'. we can find the “show string [Message]” block. Drag it inside 
the “forever” block. Finally, we type the message we want to display, for example “Hello World!”.

.. image:: /_static/images/print_string.png

You can add as many messages as you want, it will be printed sequentially.

Use icons
------------

We can also display a sequence of images on the LED matrix with little effort. In this case, we will
create a loop in which a contracted heart and an expanded heart are displayed alternately.

If we open the "Basic" section of the block menu, we will find the "show icon" block that allows us to 
display a pre-drawn image on the LED matrix of the board. Among the available options we can find the 
drawing of an expanded heart and a contracted heart.

.. image:: /_static/images/heartbeat.png

If we insert these blocks into the "forever" block, we will have a sequence of images that will be displayed alternately on 
the LED matrix until we turn off the board.

.. image:: /_static/images/heart_code.png

We can go one step further and enter a pause block which can also be found in the "Basic" section. If we know our heart rate, 
by choosing the right delays between image and image, we can simulate our own heartbeat!

.. image:: /_static/images/heart_pause.png

If you feel inspired and creative, you can use the "Show LEDs" block to create your own patterns. Don't just stick with the 
example, try creating new sequences with more than two drawings.

Light specific LEDs
------------

In order to operate on a single LED on the board matrix we must include the instruction "plot x y". This instruction can 
be found in the LED section.

.. image:: /_static/images/plot_led.png

The first number that can be modified is the column and the second the row, with them the position of the LED to be 
illuminated is indicated. Both start from 0, so in the image above the upper left LED is illuminating.

If we get tired of having one of the LEDs on and we want to turn it off we use the instruction "hide x y", which is also 
found in the LED section. If we want to see how it blinks we must add wait (100 ms for example) between each of these instructions.

.. image:: /_static/images/blink.png

Be brave and create some sequences of flashing LEDs! We encourage you to dig into some more advanced options such as adjusting the 
brightness, which can be done with the instruction "plot x and brightness", it is located in the "more" section of "LED".

