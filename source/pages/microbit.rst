Microbit
===================================

In your hands you have a micro:bit board, a microcontroller integrated in a project development board prepared for 
programming in graphic environments. This board contains actuators, sensors and input pins that we can easily program 
to customize all our projects to the maximum. These conditions make it ideal for learning programming, robotics and 
electronics. In this guide we will teach you how to use this board and its modules, starting by programming small 
example projects that will familiarize you with all its components and will serve as an approach to the graphical 
block programming environment MakeCode. We will finish by integrating the micro:bit board into the LoCoQuad robotic 
platform and teaching you how to control its servomotors and manage the information received by its sensors.

MakeCode is an open source programming environment developed by Microsoft. It allows you to program in a block editor, 
simulate the behavior of real hardware virtually and modify the code created in blocks in Python and JavaScript languages 
(or even program from scratch in these languages if you are ready). In the projects in the last section, we will show you 
how to get around this intuitive platform and get the most out of it.

Modules
------------

**LED matrix**: 25 matrix red LEDs arranged in a 5x5 distribution. They allow us to display all kinds of characters and drawings 
and even sweep letters to form complete sentences. In addition, the LEDs are sensitive to external light, generating a measurable 
voltage drop proportional to the light received. 

**Push buttons A and B**: these two buttons act as a communication interface between the board and the user, giving access to two 
manual information inputs. They also serve to switch to Bluetooth pairing mode.

**Touch button**: in addition to buttons A and B, the logo of the board is a capacitive touch sensor that functions as another manual 
information input.

**Pins**: the board has 3 analog input pins (numbered 0 to 2), one for power (labeled 3V) and one for ground (GND or ground). The rest of 
the pins are not designed to be used by novice users, but their functions are detailed and we can access them if we know what we are 
doing. To find more detailed information about the rest of the pins we can visit this guide.

**Speaker**: a buzzer type speaker that allows us to emit frequencies and even play simple music.

**Microcontroller**: this is the brain of the board. All the logic that we program is stored and executed here, and it is the one that 
sends the instructions to the rest of the modules separately. 

**USB interface**: this chip is in charge of managing the communication of the microcontroller with the computer via USB.

**USB connector**: USB port for cable connection to a computer. It is used to power the board and flash the program (record the 
desired behavior on the controller).

**RESET button**: forces a RESET on the board that makes it restart its operation as if it had just received power. It also 
serves, together with the A and B buttons, to switch to Bluetooth pairing mode.

**Microphone**: The on-board microphone is sensitive to audio levels, but is not capable of discerning frequencies. That is, it does 
not differentiate bass from treble, only loud sounds from bass, so it cannot be used for voice recognition applications.

**Power connector**: this connector allows to power the board with an external battery and it is recommended to use it when we want 
to program behaviors that require the board to be portable or not depend on the computer to be powered.

**Bluetooth antenna**: the antenna that allows the board to emit and receive wireless signals via Bluetooth.

**IMU**: the IMU (Inertial Measurement Unit) is the module that allows the board to take readings of the acceleration to which the 
board is subjected and the magnetic field in the environment.

What can you do with microbit?
------------

You can do plenty of stuff just with a microbit board. You can check some of the begginer tutorials in the section :ref:`MakeCode Tutorials <Use a bluetooth connection>`.
For its integrated use with your robot, check the examples at :ref:`Move your robot <Move your robot>`.