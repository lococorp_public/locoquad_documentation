Press your buttons
===================================

Here we will learn how to use the A-B buttons and the touch sensor to interact with the board.

A-B buttons
------------

As a example, we are going to make use of the A and B pushbuttons to implement a counter 
that increases in value when we press the A button and decreases when we press the B button. 

First, we create a variable that stores the value of the counter. This variable will remember 
the last value we have given to the counter and will modify its value when we order it. For it, we 
go to the section "Variables" and we create a new variable with the name that we want, but it is advisable 
to use a name that describes the function that it is going to fulfill, "Counter", for example. We use "set [Variable] 
to [Value]" to make sure that the counter starts at 0. Plug it inside the 'On start' block, because
variables are declared at the beggining of the program.

.. image:: /_static/images/variable.png

Next, we program the LED matrix to display the value of the counter. To do this, inside 
the "forever" block we are going to insert the "show number" block, since our counter variable is always going to 
store numeric values. If the variable were to store character strings, we could use the "show string" block instead. 
Both are found in the "Basic" section. It is important to make sure that in any case what we show is the value of the 
variable we are using, in this case "Counter". Drag the 'Counter' variable from the 'Variables' section into the 
the 'show number' block.

.. image:: /_static/images/show_variable.png

Now we need to establish the A-B buttons as events to trigger the increase or decrease of the 'Counter' value.
The event detection blocks are in the "Input" section. In this case we are interested in detecting when a button A or B 
has been pressed, so we will use two blocks of type "when button [Button] is pressed" to process button A and B presses.

Inside these blocks we must put the blocks that process the events. In this case, inside the block "when button A is pressed" 
we will include a block that allows us to increase the value of the Counter variable by 1. This block can be found in the 
"Variables" section and is called "change [Variable] by [Value]", which adds to the current value of the variable the value 
that we choose (note: it does not replace the current value of the variable with a new one). This same block can be used to 
subtract from the Counter variable, by simply changing the value of the block to -1. 

.. image:: /_static/images/press_AB.png

The counter already fulfills the desired operation, but if we want to extend a little, we can make, for example, 
that the counter returns to 0 when the buttons A and B are pressed simultaneously.

.. image:: /_static/images/A+B.png

We can also include a limiter that does not allow subtracting below 0 or adding above 9 so the numbers displayed 
always fit in the LED matrix. We will do it with conditional instructions. You can find them in the 'Logic' section.

Conditionals work as follows. If the condition is fuilfilled, the code inside the 'If' block will be executed. Otherwise,
it won't. In our case, our first condition is to decrease the value only if counter if greater than 0. We would usetwo blocks
from the conditional blocks, as you can see in the next image. The second one is the one the performs the comparison
between 'Counter' and 0. You need to plug in inside the conditional block. You will also need to plug the 'Counter' variable
inside the comparison block.

.. image:: /_static/images/conditionals.png

You can further extend the counter operation with any behavior you can think of!

Touch sensor
------------

You can also use the hidden button on the board: the touch sensor on the micro:bit icon. We will reproduce a melody
when the touch sensor is pressed, and a different one if we pressed button A instead.

Similarly to the previous example, we will find the event detection blocks in the "Input" section. In this case we will use 
the 'on logo [pressed]' and the 'on button [A] pressed' blocks. For reproducing melodies, go to the 'Music' section and drag 
the 'play melody' block. You can select the melody by clicking into the music box inside the block. Your code should look similar 
to the one in the image.

.. image:: /_static/images/touch_sensor.png

Now you can combine both the buttons and the touch sensor to create different behaviours.
