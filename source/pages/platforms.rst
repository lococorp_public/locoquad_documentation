Extend your LoCoQuad with other boards
===================================
LoCoCarrier is the board designed to host peripherals and the brain board, which can
be any micro:bit compatible board, as the LoCoBoard, or any other microcontroller
connected via I2C. It has 12 PWM connectors powered to 5V, 1 I2C connector powered to
3V3 and another I2C connector powered to 5V.

Using an I2C connected board as brain
------------
You can connect any microcontroller you desire to the LoCoCarrier, although the best
method is to host a micro:bit compatible board in the micro:bit connector, since it
will take care of the board placement and gives us acces to UART and SPI interfaces.
If we want to connect any other board via I2C, we can use one of the two exposed I2C
connectors. If the chosen board is powered by 5V, it must be connected to the J7
connector, while if the board is powered by 3.3V, it must be connected to the J4
connector. From there, you already have access to your favorite programming languages
(compatible with your board) and the other I2C devices connected to the LoCoCarrier.

Raspberry Pi 4
------------

Raspberry Pi 4 works at 5V supply voltage, so you will have to use the 5V I2C
connector. From there, take the ground wire (the first one) and connect it to any
ground pin on your Raspberry Pi 4 (pins 6, 9, 20, 25, 30, 34, 39). Then connect the
second wire, which will be the supply voltage, to either pin 2 or 4 of your Raspberry
Pi 4. Make sure you don't missconnect this wire since it can damage your Raspberry Pi.
Finally connect the SDA and SCL wires from the I2C interface to pins 8 and 9,
respectively. It is done, you have your Raspberry Pi 4 ready to program and rule over
the LoCoQuad robot.

.. image:: _static/raspi.png
  :width: 720

Programming PCA9685 in Arduino
------------

If the selected board is an Arduino model or any Arduino programmable board, we might
want to know how to program PCA9685 on Arduino language. We will use Adafruit PWM
servo driver library, which we can learn how to install and use on this `link <https://learn.adafruit.com/16-channel-pwm-servo-driver/using-the-adafruit-library>`_. Then,
configuring our servo array is simple, we just have to specify the I2C address like
this:

.. code-block:: c

    Adafruit_PWMServoDriver servos = Adafruit_PWMServoDriver(0x40);

From there, you can use servos object to set a PWM signal on the servos with the
setPWM(servo_address, start, end). Make sure to include on your setup cycle the
following instructions:

.. code-block:: c

    servos.begin(); //Setups the I2C interface and hardware
    servos.setPWMFreq(FREQ); //Sets servos operating frequency

The only thing left is understanding the setPWM() method. First, servo address just
refers to one of the 16 output lines of the PCA9685 from 0 to 15. Obviously the
available lines are actually from 0 to 11 since there's no more connected devices. The
start parameter is the instant we want the signal to start from the beginning of the
PWM cycle. We will leave it fixed at 0 (the very beginning). On the other side, the
end parameter is the instant we want the signal to end from the start of the PWM
cycle. Since we've left start on 0, we can set the end equal to the desired PWM width.
We recommend to map every angle to PWM signals using the angle and PWM width limits of
our servos, like so:

.. code-block:: c

    #define DUTY(X) map(X, ANG_MIN, ANG_MAX, MIN_PWM, MAX_PWM)

This way, setting the angle for a servo is easy:

.. code-block:: c

    servos.setPWM(servo_address, 0, DUTY(angle));


Programming PCA9685 in Python
------------

If you are rather a Python programmer, no problemo, we got you covered. In this case
we are using Adafruit_PCA9685 library. Details on installation can be found `here <https://github.com/adafruit/Adafruit_CircuitPython_PCA9685>`_.
Similarly to Arduino usage, first we have to configure the servo array:

.. code-block:: python

    pwm = Adafruit_PCA9685.PCA9685(address=0x40, busnum=2)

As you can see, this time we've specified also the bus number, since PocketBeagle (the
board used on the demo) has two I2C channels. Make sure you are configuring both the
address and bus correctly.
Once again we have to remember to set the PWM frequency to the servo operating
frequency:

.. code-block:: python

    pwm.set_pwm_freq(50)

And with that, we are set. We don't need anything more to use the set_pwm() method:

.. code-block:: python

    pwm.set_pwm(servo_channel, start, end)

As you can see, the method works exactly the same as in the Arduino example, and we
once again recommend you to make some method to help you map the angles in PWM pulses,
setting the start to 0 and the end to the PWM width:

.. code-block:: python

    def set_servo_angle(channel, angle):
      pulse = int(angle * ((servo_max-servo_min)/180)) + servo_min
      print('{0} pulse width, {1} angle'.format(pulse, angle))
      pwm.set_pwm(channel, 0, pulse)